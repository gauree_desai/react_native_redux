import React from 'react'
import { View, Text, StyleSheet,Image } from 'react-native'
import {  SafeAreaView } from 'react-native-safe-area-context';

export default function ListItem({item,key}) {
    return (
       
        <SafeAreaView styles={styles.listItem}key={key}>
           <Image source={{uri:item.image}} style={styles.placeImage}/>
            <Text styles={styles.textval}>
                {item.name}
            </Text>
        </SafeAreaView>
       
    )
}

const styles = StyleSheet.create({
    listContainer:{
        width:"80%"
      },
    listItem:{
        width:"80%",
        padding:10,
        marginTop:2,
        color:'#191970',
        backgroundColor: '#eee',
        alignSelf:'stretch',
        flex:1,
        justifyContent:'space-between',
        alignItems:'center'

    },
    placeImage:{
        marginRight:8,
        height:200
    },
    textval:{
        textAlign:'center',
        fontSize:30
    }
})
