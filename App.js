import { StatusBar } from 'expo-status-bar';
import React,{ useState} from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import List from './Components/List';
import {Provider} from 'react-redux'
import store  from './Store'
import Navbar from './Components/Nav_bar';

export default function App() {
  const [text, setText ]= useState("Hello World!!")
  return (
     
        <Provider store={store}>
        <Navbar></Navbar>
        {/* <Text style={styles.mainHeading}>Shopping Cart</Text>
        <List></List> */}

        {/* <Text>{text}</Text>
        <Button title="Change Text" onPress={()=>{setText("Changed Text")}}></Button>
          */}
        <StatusBar style="auto" />
        </Provider>
     
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainHeading:{
    marginTop:20,
    fontWeight:"bold",
    color:"red",
    fontSize:30
  }
});
